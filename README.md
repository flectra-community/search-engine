# Flectra Community / search-engine

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[connector_search_engine](connector_search_engine/) | 2.0.2.6.0| Connector Search Engine
[connector_algolia](connector_algolia/) | 2.0.2.2.0| Connector For Algolia Search Engine
[connector_elasticsearch](connector_elasticsearch/) | 2.0.2.6.0| Connector For Elasticsearch Search Engine


